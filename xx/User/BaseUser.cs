﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace xx
{
    public class BaseUser
    {

        [Display(Name="Username")]
        public string username { get; set; }

        [Display(Name = "Password")]
        public string password { get; set; } 

        public string password_salt { get; set; }


        public void HashPassword(int saltLength)
        {
            var salt = Security.PasswordHasher.Salt.Generate(saltLength);
            this.password_salt = salt;
            this.password = Security.PasswordHasher.Hash(this.password, salt);
        }

        public void sayHi()
        {
            Console.WriteLine("Hi");
        }
    }


}
