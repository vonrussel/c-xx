﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace xx
{
    public class TCPClient
    {
        public TcpClient tcpClient;

        // event callbacks
        public Action<TCPClient> OnDisconnectCallback;
        public Action<TCPClient, dynamic> OnMessageReceivedCallback =
            delegate(TCPClient client, dynamic message)
            {

            };

        private IPEndPoint _IPCache;
        public IPEndPoint RemoteEndPointIP
        {
            get {
                if (!SocketConnected())
                {
                    // used cached ip info coz client socket
                    // may be nulled once connection is closed
                    return (_IPCache != null) ? _IPCache : null;
                }
                IPEndPoint ip = tcpClient.Client.RemoteEndPoint as IPEndPoint;
                _IPCache = ip; // cache
                return ip;
            }
        }
        public IPEndPoint LocalEndPoint
        {
            get
            {
                try
                {
                    return tcpClient == null ? null : tcpClient.Client.LocalEndPoint as IPEndPoint;
                }
                catch (Exception ex) { }
                return null;
            }
        }
    



        public TCPClient(TcpClient _tcpClient)
        {
            tcpClient = _tcpClient;
        }

        public TCPClient(String host, int port)
        {
            tcpClient = new TcpClient(host, port);
        }


        public void startClient()
        {
            new Thread(doChat).Start();
        }

        private void doChat()
        {
            int requestCount = 0;

            string dataFromClient = null;
            string serverResponse = null;
            requestCount = 0;

            while (true)
            {
                try
                {
                    requestCount = requestCount + 1;
                    if (!this.SocketConnected())
                        break; // close

                    NetworkStream networkStream = tcpClient.GetStream();

                    // read the first flush of client socket
                    // first 4 byte includes the size of the stream
                    byte[] lengthBytes = new byte[4];
                    int read = networkStream.Read(lengthBytes, 0, 4);
                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(lengthBytes);

                    int length = BitConverter.ToInt32(lengthBytes, 0);
                    byte[] buf = new byte[length];
                    networkStream.Read(buf, 0, buf.Length);

                    dataFromClient = System.Text.Encoding.ASCII.GetString(buf);
                    // call external callback method
                    if (OnMessageReceivedCallback != null)
                    {
                        OnMessageReceivedCallback(this, JsonConvert.DeserializeObject(dataFromClient));
                    }

                    Console.WriteLine(" >> " + serverResponse);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(" >> " + ex.ToString());
                    break;
                }
            }

            this.Close();
        }

        private void Close()
        {
            if (OnDisconnectCallback != null)
            {
                OnDisconnectCallback(this);
            }
            tcpClient.Close();
        }

        bool SocketConnected()
        {
            if (tcpClient == null || !tcpClient.Client.Connected) return false;

            try
            {
                // test if server socket is open on remote end point
                bool part1 = tcpClient.Client.Poll(100, SelectMode.SelectRead);
                bool part2 = (tcpClient.Client.Available == 0);
                if (part2 && part1)
                {//connection is closed
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public void SendMessage(Object toSend, Action<Exception> callback)
        {
            String jsonString = JsonConvert.SerializeObject(toSend);
            byte[] streamMessage = Encoding.UTF8.GetBytes(jsonString);

            try
            {
                var stream = tcpClient.GetStream();

                // send byte size first
                byte[] size = BitConverter.GetBytes(streamMessage.Length);
                if(BitConverter.IsLittleEndian)
                    Array.Reverse(size);
                stream.Write(size, 0, size.Length);

                // write the actual message string
                stream.Write(streamMessage, 0, streamMessage.Length);
                stream.Flush();

            }
            catch (ObjectDisposedException ex)
            {
                Console.WriteLine(ex.ToString());
                if (callback != null && !ex.Equals(null))
                    callback(ex);
                return;
            }
            if(callback != null)
                callback(null);
        }
    }
}
