﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace xx
{
    public class TCPServer
    {
        TcpListener serverSocket;
        private bool acceptClients = true;
        public List<KeyValuePair<String, TCPClient>> Clients;

        // event callbacks
        public Action<TCPClient> OnTCPClientConnectedCallback;
        public Action<TCPClient> OnTCPClientDisconnectCallback;
        public Action<TCPClient, dynamic> OnMessageReceivedCallback =
            delegate(TCPClient client, dynamic message)
            {

            };
        

        public TCPServer(int port)
        {
            serverSocket = new TcpListener(port);
            Clients = new List<KeyValuePair<String, TCPClient>>();
        }

        public void Start() {
            new Thread(StartServerThreadDelegate).Start();
        }

        private void StartServerThreadDelegate()
        {
            TcpClient clientSocket = default(TcpClient);

            serverSocket.Start();
            Console.WriteLine(" >> Server Started...");

            while (true)
            {
                // start accepting clients sockets
                clientSocket = serverSocket.AcceptTcpClient();
                Console.WriteLine(" >> Client started");

                TCPClient client = new TCPClient(clientSocket);
                client.startClient();
                this.AddUpdateClient(client.RemoteEndPointIP.Address.ToString(), client);

                // callback when connected
                if (OnTCPClientConnectedCallback != null)
                {
                    OnTCPClientConnectedCallback(client);
                }

                // callback when client disconnects
                if (OnTCPClientDisconnectCallback != null)
                {
                    // pass our disconnect callback from the source
                    client.OnDisconnectCallback += OnTCPClientDisconnectCallback;
                }

                // callback when message is revceived
                if (OnMessageReceivedCallback != null)
                {
                    // pass our disconnect callback from the source
                    client.OnMessageReceivedCallback += OnMessageReceivedCallback;
                }

                if (!acceptClients) break;
            }

            clientSocket.Close();
            serverSocket.Stop();
            Clients = new List<KeyValuePair<String, TCPClient>>();
            Console.WriteLine(" >> closing server...");
        }

        public void Stop()
        {
            if (serverSocket != null)
            {
                acceptClients = false;
            }
        }

        public void AddUpdateClient(String ipString, TCPClient client)
        {
            this.Clients.RemoveAll((cl) => cl.Key == ipString);
            this.Clients.Add(new KeyValuePair<String, TCPClient>(ipString, client));
        }
            
    }
}
