﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Collections;

namespace xx
{
    public class Network
    {
        public static IEnumerable<IPAddress> GetLocalIP()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    yield return ip;
                }
            }
        }

        public static String ByteIPToString(byte[] ip)
        {
            return (new IPAddress(ip)).ToString();
        }

        public static void FindLocalNetworkIpAddress(Action<IPAddress> iteratorCallback)
        {
            List<IPAddress> localIps = new List<IPAddress>(GetLocalIP());
            for (int j = 0; j < localIps.Count; j++)
            {
                var localIp = localIps.ElementAt(j).GetAddressBytes();
                for (int i = 1; i <= 254; i++)
                {
                    localIp[3] = (byte)i;
                    var newIP = new IPAddress(localIp);
                    // add iterator callback to allow external 
                    // calls in separate thread
                    iteratorCallback(newIP);
                }
            }
        }

        public static String IpToString(IPEndPoint ipEndpoint)
        {
            String ip = "";
            try
            {
                return ipEndpoint.Address.ToString();
            }
            catch (Exception ex) { }
            return ip;
        }



    }
}
