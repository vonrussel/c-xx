﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace xx.Security
{
    public class PasswordHasher
    {

        public static string Hash(string _password, string _salt)
        {
            byte[] password = Encoding.UTF8.GetBytes(_password);
            byte[] salt = Encoding.UTF8.GetBytes(_salt ?? "");

            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainTextWithSaltBytes =
                new byte[password.Length + salt.Length];

            for (int i = 0; i < password.Length; i++)
            {
                plainTextWithSaltBytes[i] = password[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[password.Length + i] = salt[i];
            }

            return Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes));
        }


        public static class Salt
        {
            public static string Generate(int length)
            {
                return xx.String_.GenerateRandom(length);
            }
        }
    }
}
